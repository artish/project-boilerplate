#!/bin/sh
# Set up the boilerplate for your project

echo "Project Name:"
read PROJECT_NAME

# Remove the boilerplate git file
rm -rf .git/

# Rename the sublime project
mv project.sublime-project $PROJECT_NAME.sublime-project

# Install bower and npm packages
bower install
npm install