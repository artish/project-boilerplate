/*jshint node: true*/

'use strict';

// Requirements
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    runSequence = require('run-sequence');

var config = require('./bower_components/gulp-boilerplate/modules/loadGulpConfig.js')('gulp/gulpconfig.json');
var paths = config.paths;

/*-------------------------------------------------------*\
* Tasks
\*-------------------------------------------------------*/

var tasks = [
  'browsersync.js',
  'sass.js',
  'jade.js',
  'lintjs.js',
  'browserify.js',
  'images.js',
  'svg.js',
  'clean.js'
];

tasks.forEach(function(x) {
  require('./bower_components/gulp-boilerplate/tasks/' + x)(config);
});

/*-------------------------------------------------------*\
* Watch
\*-------------------------------------------------------*/

gulp.task('watch', ['reload', 'browserSync'], function() {
  gulp.watch((paths.img.src + paths.img.files), ['images']);
  gulp.watch((paths.js.src + paths.js.files), ['lintjs']);
  gulp.watch((paths.templates.src + paths.templates.files), ['templates']);
  gulp.watch((paths.css.src + paths.css.files), ['sass']);
});

/*-------------------------------------------------------*\
* Task Bundles
\*-------------------------------------------------------*/

// Compile all files
gulp.task('compile', [
  'sass',
  'scripts',
  'images',
  'svg',
  'templates'
]);

// Continous Compilation
gulp.task('default', [
  'compile',
  'watch'
]);

gulp.task('test', []);

// Run sequence runs tasks sequentially so you don't get an error
// when your delete the dst folder at the beginning
gulp.task('build', function(callback) {
  runSequence('clean', ['compile'], callback);
});
