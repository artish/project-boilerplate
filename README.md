Project Boilerplate
===================

Bootstrap your projects with this boilerplate!

## Requirements

  [git], [node], [bower], [gulp]

  A Global Gulp Installation:
  `npm install --global gulp`

## Installation

  If you've got all the requirements installed,    
  run `bash setup.sh` script to start your project.

[git]: https://git-scm.com/
[node]: https://nodejs.org/en/
[bower]: http://bower.io/
[gulp]: http://gulpjs.com/